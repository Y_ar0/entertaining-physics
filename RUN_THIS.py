# -*- coding: utf8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tkinter as tk
from tkinter import filedialog as fd
import tkinter.messagebox as mb
from tkinter.ttk import *
from PIL import Image, ImageTk
import webbrowser
# import os, sys
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.animation as animation

g = 9.81523


class PlotXYT(tk.Toplevel):
    def __init__(self):
        super().__init__()

        self.resizable(False, False)
        w = ((self.winfo_screenwidth() // 2) - 600)
        h = ((self.winfo_screenheight() // 2) - 337)
        self.geometry('1200x675+{}+{}'.format(w, h))
        self.wm_title('Проекции движения тела')

        fig = plt.Figure(figsize=(8, 6))
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.get_tk_widget().grid(row=0, column=0, columnspan=8, rowspan=8)

        Label(self, text=' Для построения графика вы можете: ', justify='left', background='white', font='Helvetica 12').grid(row=1, column = 2,  sticky='w')
        Label(self,text='      -ввести данные в окошки', justify='left', background='white', font='Helvetica 12').grid(row=2, column=2, sticky='w')
        Label(self, text='      -загрузить csv-файл с координатами (t, x, y)', justify='left', background='white', font='Helvetica 12').grid(
            row=3, column=2, sticky='w')
        Label(self, text='      -загрузить txt-файл c входными параметрами (Vo, угол α, Xo, Yo) ', justify='left', background='white', font='Helvetica 12').grid(
            row=4, column=2, sticky='w')

        btn_file = tk.Button(self, text="Выбрать файл и\n построить график", justify='center', bg = '#387294',fg="white",  command=lambda: PlotXYT.make_plot(self, PlotXYT.choose_file(self)))
        btn_file.grid(row=1, column=13, columnspan = 2, padx=60, pady=2)

        Label(self, text =' или введите значения: ', justify='center').grid(row=2, column=13, columnspan = 2, padx = 10)
        Label(self, text = ' Начальная скорость (Vo) в м/с: ').grid(row = 3, column = 13, sticky = 'w', padx = 10, pady = 2)
        Label(self, text = ' Угол к горизонту (α) в градусах: ').grid(row=4, column=13, sticky='w', padx=10, pady=2)
        Label(self, text = ' Начальное положение (Xo) в метрах :  ').grid(row=5, column=13, sticky='w', padx=10, pady=2)
        Label(self, text = ' Начальная высота (Yo) в метрах: ').grid(row=6, column=13, sticky='w', padx=10, pady=2)

        btn_do_plot = tk.Button(self, text="Построить график\n по заданным значениям ", bg = '#387294',fg="white", command=lambda: PlotXYT.make_plot(self, PlotXYT.param(self)))
        btn_do_plot.grid(row=7, column=13, columnspan=2, padx=60, pady=2)

        self.Entry_V0 = Entry(self, width=8, font='Helvetica 12')
        self.Entry_V0.grid(row=3, column=14, sticky='w')
        self.Entry_a = Entry(self, width=8, font='Helvetica 12')
        self.Entry_a.grid(row=4, column=14, sticky='w')
        self.Entry_x0 = Entry(self, width=8, font='Helvetica 12')
        self.Entry_x0.grid(row=5, column=14, sticky='w')
        self.Entry_y0 = Entry(self, width=8, font='Helvetica 12')
        self.Entry_y0.grid(row=6, column=14, sticky='w')

    def make_plot(self, data):
        if np.array_equal(data, [None, None, None]):
            msg = u'  Невозможно построить график \n  Возможные проблемы:\n    -Вы ввели не все значения\n    -Значения введены некорректно\n    -Вы не выбрали файл для построения графика\n    -Данные в файле некорректны\n       ("Требование к загружаемым данным" в главном меню) '
            mb.showwarning("Предупреждение", msg)
        else:
            x, y, t = data
            Label(self,
                  text='  Загрузите файлы:\n          .csv с координатами или (t, x, y) \n          .txt c входными параметрами (Vo, угол α, Xo, Yo)',
                  justify='left').grid(row=0, column=13, columnspan = 2)

            btn_download_file = tk.Button(self, text="Сохранить полученные координаты", justify='center',  bg = '#387294', fg="white", command=lambda: PlotXYT.save_data(self, PlotXYT.param(self)))
            btn_download_file.grid(row=8, column=13, columnspan=2, padx=60, pady=2)

            fig = plt.Figure(figsize=(8, 6))
            gs = fig.add_gridspec(2, 2)

            ax1 = fig.add_subplot(gs[0, :])
            ax2 = fig.add_subplot(gs[1, 0])
            ax3 = fig.add_subplot(gs[1, 1])

            ax2.set_title('График зависимости X от времени T', fontsize=12)
            ax3.set_title('График зависимости Y от времени T', fontsize=12)

            ax1.grid(True)
            ax2.grid(True)
            ax3.grid(True)

            ax1.plot([], [], color='maroon')
            ax2.plot(t, x, linestyle='--', linewidth=2)
            ax3.plot(t, y, linestyle='--', linewidth=2)
            miny = min(y)
            maxy = max(y)
            if self.Entry_a.get()==90:
                minx=self.Entry_x0.get()-1
                maxx = self.Entry_x0.get()+1
            else:
                minx=min(x)
                maxx=max(x)

            canvas = FigureCanvasTkAgg(fig, master=self)
            canvas.get_tk_widget().grid(row=0, column=0, columnspan=8, rowspan=8)

            self.ani = animation.FuncAnimation(fig, lambda i: PlotXYT.animate(i, ax1, miny, maxy, minx, maxx, x, y), frames=len(t), blit=False, repeat=False)

    @staticmethod
    def animate(i, ax1, miny, maxy, minx, maxx, x, y):
        ax1.clear()
        ax1.set_title('Траектория полета', fontsize=14)
        ax1.plot(x[:i], y[:i], color='lightblue')
        ax1.plot([x[i]], [y[i]], 'ro')
        ax1.grid(True)
        ax1.set_ylim([miny, maxy + (maxy - miny) / 10])
        ax1.set_xlim([minx, maxx+ (maxx - minx) /20])
        # ax1.text()
        return ax1

    def param(self):
        data: [str] = [self.Entry_V0.get(),
                       self.Entry_a.get(),
                       self.Entry_x0.get(),
                       self.Entry_y0.get()]
        if '' in data:
            return None, None, None
        else:
            for i in range(4):
                if data[i] != '':
                    data[i] = float(data[i].replace(',', '.'))
                else:
                    data[i] = None
            v0x = data[0] * np.cos(np.radians(data[1]))
            v0y = data[0] * np.sin(np.radians(data[1]))
            T = (v0y + np.sqrt(v0y ** 2 + 2 * g * data[3])) / g
            t = np.linspace(0, T, 100)
            x = data[2] + v0x * t
            y = data[3] + v0y * t - g * (t ** 2) / 2
            return x, y, t

    def choose_file(self):
        filename = fd.askopenfilename(title="Открыть файл", initialdir="/")
        if filename != '':
            f = open(filename, 'r', encoding='utf-8')
            if filename.endswith('.csv'):
                # на вход t x y
                require_cols = [0, 1, 2]
                dt_csv = pd.read_csv(f, sep=';', usecols=require_cols)
                f.close()
                if dt_csv['t'].dtype=='float64' or dt_csv['t'].dtype=='int64':
                    x = dt_csv['x']
                    y = dt_csv['y']
                    t = dt_csv['t']
                    return x, y, t
                else:
                    for name in ['t', 'x', 'y']:
                        dt_csv[name] = dt_csv[name].str.replace(',', '.')
                    dt_csv = dt_csv.apply(pd.to_numeric)
                    x = dt_csv['x']
                    y = dt_csv['y']
                    t = dt_csv['t']
                    return x, y, t

            elif filename.endswith('.txt'):
                # на вход: v0 a x0 y0
                dt_txt = list(map(float, f.read().replace(',', '.').split()))
                f.close()
                v0x = dt_txt[0] * np.cos(np.radians(dt_txt[1]))
                v0y = dt_txt[0] * np.sin(np.radians(dt_txt[1]))
                T = (v0y + np.sqrt(v0y ** 2 + 2 * g * dt_txt[3])) / g
                t = np.linspace(0, T, 100)
                x = dt_txt[2] + v0x * t
                y = dt_txt[3] + v0y * t - g * (t ** 2) / 2

                self.Entry_V0.insert(0, dt_txt[0])
                self.Entry_a.insert(0, dt_txt[1])
                self.Entry_x0.insert(0, dt_txt[2])
                self.Entry_y0.insert(0, dt_txt[3])
                return x, y, t
            else:
                return None, None, None
        else:
            return None, None, None

    def save_data(self, data):
        x, y, t = data
        dict = {'t': t, 'x': x, 'y': y}
        df = pd.DataFrame(dict)
        new_file = fd.asksaveasfile(title="Сохранить файл", defaultextension=".csv",
                                    filetypes=(("csv - файл", "*.csv"),))
        if new_file:
            df.to_csv(new_file, index=False, sep=";")
            new_file.close()


class BigPlot(tk.Toplevel):
    def __init__(self):
        super().__init__()

        self.resizable(False, False)
        w = ((self.winfo_screenwidth() // 2) - 600)
        h = ((self.winfo_screenheight() // 2) - 337)
        self.geometry('1200x675+{}+{}'.format(w, h))
        self.wm_title('Изменение проекций скорости')


        fig = plt.Figure(figsize=(8, 6))
        canvas = FigureCanvasTkAgg(fig, master=self)
        canvas.get_tk_widget().grid(row=0, column=0, columnspan=8, rowspan=6)

        Label(self, text=' Введите данные в окошки справа ',
              justify='left', background='white', font='Helvetica 16').grid(row=2, column = 2,  sticky='w')

        Label(self, text =' Введите значения: ', justify='center').grid(row=0, column=13, columnspan = 2, padx = 10, pady=2)
        Label(self, text = ' Начальная скорость (Vo) в м/с: ').grid(row =1, column = 13, sticky = 'w', padx = 10, pady = 2)
        Label(self, text = ' Угол к горизонту (α) в градусах: ').grid(row=2, column=13, sticky='w', padx=10, pady=2)
        Label(self, text = ' Начальное положение (Xo) в метрах : ').grid(row=3, column=13, sticky='w', padx=10, pady=2)
        Label(self, text = ' Начальная высота (Yo) в метрах: ').grid(row=4, column=13, sticky='w', padx=10, pady=2)

        btn_do_plot = tk.Button(self, text="Построить график\n по заданным значениям ", bg = '#387294',fg="white", command=lambda: BigPlot.make_plot(self, BigPlot.param(self)))
        btn_do_plot.grid(row=5, column=13, columnspan=2, padx=60, pady=2)

        self.Entry_V0 = Entry(self, width=8, font='Arial 12')
        self.Entry_V0.grid(row=1, column=14, sticky='w')
        self.Entry_a = Entry(self, width=8, font='Arial 12')
        self.Entry_a.grid(row=2, column=14, sticky='w')
        self.Entry_x0 = Entry(self, width=8, font='Arial 12')
        self.Entry_x0.grid(row=3, column=14, sticky='w')
        self.Entry_y0 = Entry(self, width=8, font='Arial 12')
        self.Entry_y0.grid(row=4, column=14, sticky='w')

    def make_plot(self, data):
        if np.array_equal(data, [None, None, None, None, None, None, None, None, None]):
            msg = u'  Невозможно построить график \n  Возможные проблемы:\n    -Вы ввели не все значения\n    -Значения введены некорректно\n    -Вы не выбрали файл для построения графика\n    -Данные в файле некорректны\n       ("Требование к загружаемым данным" в главном меню) '
            mb.showwarning("Предупреждение", msg)
        else:
            x, y, t, vx,  vy, v, T, H, L = data
            Label(self, text=' Т = {}(с)    H = {}(м)   L = {}(м) '.format(T, H, L), justify='center', font='16', background='white').grid(row=0, column=0,columnspan=12,padx=10,pady=6)
            btn_download_file = tk.Button(self, text="Сохранить полученные координаты", justify='center' , bg = '#387294', fg="white", command=lambda: BigPlot.save_data(self, BigPlot.param(self)))
            btn_download_file.grid(row=6, column=13, columnspan=2, padx=60, pady=2)

            fig = plt.Figure(figsize=(8, 6))
            ax1 = fig.add_subplot(231)
            ax2 = fig.add_subplot(232)
            ax3 = fig.add_subplot(233)
            ax4 = fig.add_subplot(234)
            ax5 = fig.add_subplot(235)
            ax6 = fig.add_subplot(236)
            plt.subplots_adjust(wspace=1, hspace=1)

            ax1.set_title('X от времени T', fontsize=11)
            ax2.set_title('Y от времени T', fontsize=11)
            ax3.set_title('Траектория полета тела', fontsize=11)
            ax4.set_title('Vx от времени T', fontsize=11)
            ax5.set_title('Vy от времени T', fontsize=11)
            ax6.set_title('V мгновенное от времени T', fontsize=11)

            ax1.grid(True)
            ax2.grid(True)
            ax3.grid(True)
            ax4.grid(True)
            ax5.grid(True)
            ax6.grid(True)

            ax1.plot(t, x, color='maroon')
            ax2.plot(t, y, color='maroon')
            ax3.plot(x, y, color='maroon')
            ax4.plot(t, vx, linestyle='-.', color='mediumblue', linewidth=2)
            ax5.plot(t, vy, linestyle='-.', color='mediumblue', linewidth=2)
            ax6.plot(t, v, linestyle='-.', color='mediumblue', linewidth=2)

            canvas = FigureCanvasTkAgg(fig, master=self)
            canvas.draw()
            canvas.get_tk_widget().grid(row=1, column=0, columnspan=8, rowspan=6)

    def param(self):
        data: [str] = [self.Entry_V0.get(),
                           self.Entry_a.get(),
                           self.Entry_x0.get(),
                           self.Entry_y0.get()]
        if '' in data:
            return None, None, None, None, None, None, None, None, None
        else:
            for i in range(4):
                if data[i] != '':
                    data[i] = float(data[i].replace(',', '.'))
                else:
                    data[i] = None

            v0x = data[0] * np.cos(np.radians(data[1]))
            v0y = data[0] * np.sin(np.radians(data[1]))
            T = (v0y + np.sqrt(v0y ** 2 + 2 * g * data[3])) / g
            t = np.linspace(0, T, 100)
            vx = v0x - t*0
            vy = v0y - g*t
            v = np.sqrt(vx**2+vy**2)
            x = data[2] + v0x * t
            y = data[3] + v0y * t - g * (t ** 2) / 2
            T = round(T, 3)
            H = round(data[3]+(((data[0]*np.sin(np.radians(data[1])))**2)/(2*g)), 3)
            L = round(v0x*T, 3)
            return x, y, t, vx,  vy, v, T, H, L

    def save_data(self, data):
        x, y, t, vx,  vy, v = data
        dict = {'t': t, 'x': x, 'y': y, 'vx': vx, 'vy': vy, 'v': v}
        df = pd.DataFrame(dict)
        new_file = fd.asksaveasfile(title="Сохранить файл", defaultextension=".csv",
                                    filetypes=(("csv - файл", "*.csv"),))
        if new_file:
            df.to_csv(new_file, index=False, sep=";")
            new_file.close()


class Tasks(tk.Toplevel):
    def __init__(self):
        super().__init__()

        self.resizable(False, False)
        # self.attributes("-topmost", True)
        self.wm_title('Нахождение параметров')
        self.config(bg="white")
        Label(self, text='  Введите значения. \n Величину, которую надо найти, оставьте незаполненной.\n ',
              justify='center', background="white", font=("Verdana", 9)).grid(row=0, sticky='w', columnspan=2)

        Label(self, text=' Максимальная высота подъема (H) в метрах:  ', justify='right', background="white").grid(row=1, sticky='w')
        Label(self, text=' Максимальная длина полёта (L) в метрах:', justify='right', background="white").grid(row=2, sticky='w')
        Label(self, text=' Полное время полета (Т) в секундах:  ', justify='right', background="white").grid(row=3, sticky='w')
        Label(self, text=' Угол к горизонту (α) в градусах: ', justify='right', background="white").grid(row=4, sticky='w')
        Label(self, text=' Начальная высота (у0) в метрах: ', justify='right', background="white").grid(row=6, sticky='w')
        Label(self, text='  ', justify='right', background="white").grid(row=7, sticky='w')
        Label(self, text='  ', justify='right', background="white").grid(row=9, sticky='w')

        self.Entry_H = Entry(self, width=8, font='Verdana 11')
        self.Entry_L = Entry(self, width=8, font='Verdana 11')
        self.Entry_T = Entry(self, width=8, font='Verdana 11')
        self.Entry_alpha = Entry(self, width=8, font='Verdana 11')
        self.Entry_y0 = Entry(self, width=8, font='Verdana 11')

        self.Entry_H.grid(row=1, column=1, sticky='w')
        self.Entry_L.grid(row=2, column=1, sticky='w')
        self.Entry_T.grid(row=3, column=1, sticky='w')
        self.Entry_alpha.grid(row=4, column=1, sticky='w')
        self.Entry_y0.grid(row=6, column=1, sticky='w')

        but_calc = tk.Button(self, text=' Вычислить ', bg='#387294', fg="white", padx=5, command=lambda: self.find())
        but_calc.grid(row=8, column=1, sticky='w')
        but_clean = tk.Button(self,  text=' Очистить ', bg='#387294', fg="white", padx=5, comman=lambda: self.clean())
        but_clean.grid(row=8, column=0, sticky='e', padx=5)

    def find(self):
        data: [str] = [self.Entry_H.get(),
                       self.Entry_L.get(),
                       self.Entry_T.get(),
                       self.Entry_alpha.get(),
                       self.Entry_y0.get()]
        count=0
        for i in range(5):
            if data[i] != '':
                data[i] = float(data[i].replace(',', '.'))
            else:
                data[i] = None
                count += 1
        if count == 1:
            if data.index(None) == 0:
                self.Entry_H.insert(0, round(data[4] + ((data[1]** 2) * (np.tan(np.radians(data[3]))) ** 2 / (2 * g * data[2] ** 2)), 3))
            elif data.index(None) == 1:
                self.Entry_L.insert(0, round((data[2] * np.sqrt((data[0] - data[4]) * 2 * g)) / (np.tan(np.radians(data[3]))), 3))
            elif data.index(None) == 2:
                self.Entry_T.insert(0, round((data[1] * np.tan(np.radians(data[3]))) / (np.sqrt((data[0] - data[4]) * 2 * g)), 3))
            elif data.index(None) == 3:
                self.Entry_alpha.insert(0, round(np.rad2deg(np.arctan(data[2] * np.sqrt((data[0] - data[4]) * 2 * g) / data[1])), 3))
            elif data.index(None) == 4:
                self.Entry_y0.insert(0, round(data[0] - (((data[1] * np.tan(np.radians(data[3]))) ** 2) / (2 * g * data[2] ** 2)),3))

        else:
            msg = " Значения заданы некорректно. \n Все значения, кроме искомого, должны быть введены. "
            mb.showwarning("Предупреждение", msg)

    def clean(self):
        self.Entry_H.delete(0, 'end')
        self.Entry_L.delete(0, 'end')
        self.Entry_T.delete(0, 'end')
        self.Entry_alpha.delete(0, 'end')
        self.Entry_y0.delete(0, 'end')


class MainMenu(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("EnjoyPhysics")
        w = ((self.winfo_screenwidth() // 2) - 300)
        h = ((self.winfo_screenheight() // 2) - 400)
        self.geometry('600x800+{}+{}'.format(w, h))
        self.resizable(False, False)

        self.config(bg="white")
        label = tk.Label(self, text="Добро пожаловать в приложение НеСкучная Физика!\n \nВыберите опцию для продолжения:", background ='white', font='18')
        label.place(relx=.5, rely=.1, anchor="c", relheight=.1, relwidth=.9, bordermode='outside')

        btn = tk.Button(self, text="График траектории", bg = '#387294', font="15", fg="white", command=lambda: PlotXYT()).place(relx=.5, rely=.25, anchor="c", height=40, width=220, bordermode='outside')

        btn = tk.Button(self, text="График скорости", bg = '#387294', font="15",  fg="white",command = lambda: BigPlot()).place(relx=.5, rely=.35, anchor="c", height=40, width=220, bordermode='outside')

        btn = tk.Button(self, text="Решение задач", bg = '#387294', font="15",fg="white",  command=lambda: Tasks()).place(relx=.5, rely=.45, anchor="c", height=40, width=220, bordermode='outside')

        btn = tk.Button(self, text="Теория", bg = 'white', font="15",fg="#387294", command=lambda: open_browser("https://educon.by/index.php/materials/phys/kinematika")).place(relx=.5, rely=.55, anchor="c", height=40, width=220, bordermode='outside')

        btn = tk.Button(self, text="Требование к \nзагружаемым данным", bg = 'white', font="15",fg="#387294", command=lambda: requirements()).place(relx=.5, rely=.65,
                                                                                                  anchor="c", height=50,
                                                                                                  width=220,
                                                                                                  bordermode='outside')
        btn = tk.Button(self, text="Выход", bg = 'mistyrose', fg = "black", font="16", command=self.destroy).place(relx=.5, rely=.8, anchor="c", height=40, width=150, bordermode='outside')


        def open_browser(url):
            webbrowser.open_new(url)

        def requirements():
            root = tk.Toplevel()

            root.title('Требования к загружаемым файлам')
            root.geometry('600x800')
            root.config(bg="white")
            self.resizable(False, False)

            Label(root, text="      Программа может обрабатывать файлы со следующими расширениями: ", background='white').place(rely=.07, bordermode='outside')

            Label(root, text=" .csv ", background='white', font='bold').place(relx=.5, rely=.15, anchor="c", bordermode='outside')
            Label(root, text='      Загружаемый файл содержит 3 столбца с данными: t, x, y\n      Разделитель - ";" (точка с запятой)', background='white').place(rely=.2, bordermode='outside')
            Label(root, text="  Пример: ", background='white').place(relx=.5, rely=.28, anchor="c", bordermode='outside')
            tkimage1 = ImageTk.PhotoImage(Image.open('1.png'))
            img_label = Label(root, image=tkimage1)
            img_label.image = tkimage1
            img_label.place(relx=.5, rely=.42, anchor="c")

            Label(root, text=" .txt ",background='white', font='bold').place(relx=.5, rely=.57, anchor="c")
            Label(root, text='      Загружаемый файл содержит одну строку с данными: Vo, α, Xo, Yo\n      Разделитель - " " (пробел)\n      Нецелочисленные значения должны быть введены через . (точку)', background='white').place(rely=.62, bordermode='outside')
            Label(root, text="  Пример: ", background='white').place(relx=.5, rely=.74, anchor="c",bordermode='outside')
            tkimage1 = ImageTk.PhotoImage(Image.open('2.png'))
            img_label = Label(root, image=tkimage1)
            img_label.image = tkimage1
            img_label.place(relx=.5, rely=.86, anchor="c")


if __name__ == "__main__":
    app = MainMenu()
    app.mainloop()